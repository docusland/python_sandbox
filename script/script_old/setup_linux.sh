#!/bin/bash

# check if python is installed on the host
function check_python () {
  python_commands=('python' 'python3' 'py')
  for i in "${python_commands[@]}"
  do
      OUTPUT=$(command -v ${i})
      if [ -n "${OUTPUT}" ];
      then
#        echo ${i} "found"
        MAIN_PYTHON_FOUND="${i}"
      fi
  done
}

# create venv
function setup_env () {
  command cd api
#  echo ${MAIN_PYTHON}
#  ${MAIN_PYTHON} -V
#  ${MAIN_PYTHON} -m pip -V
  ${MAIN_PYTHON} -m virtualenv venv
  ${MAIN_PYTHON} -m pip list
#  ${MAIN_PYTHON} -m pip install -r requirements.txt
}


# get an avalaible python command
check_python
MAIN_PYTHON=$MAIN_PYTHON_FOUND

# exit if no avalaible python, else create env and run project
[[ -n "${MAIN_PYTHON}" ]] && setup_env ${MAIN_PYTHON} || { echo "Quitting, not python found, please install it"; exit 1; }

