#!/usr/bin/bash
function check_python {
  echo 'finding python command'
  python_commands=('python' 'python3' 'py')
  for i in "${python_commands[@]}"
  do
      OUTPUT=$(command -v ${i})
      if [ -n "${OUTPUT}" ];
      then
        echo ${i} "found"
        MAIN_PYTHON_FOUND="${i}"
      fi
  done
}

function find_py_project {
  ROOT='..'
  cd $ROOT || exit
#  declare -g -A ALL_PROJECT_FOUND
  declare -g -A PY_PROJECT_FOUND
  for project in `ls`
  do
    entrypoint='main_'$project'.py'
#    echo $entrypoint
    found_entrypoint=`ls $project | grep $entrypoint`
    [[ -n "${found_entrypoint}" ]] && PY_PROJECT_FOUND["$project"]=$found_entrypoint
  done
}

function create_env {
  echo 'Creating venv for' $1 'project'
  pwd
  DIRECTORY=$1
  PYTHON=$2
  cd $DIRECTORY
  ${PYTHON} -m virtualenv venv
  source venv/bin/activate
  ${PYTHON} -m pip list
  ${PYTHON} -m pip install -r requirements.txt
}

function create_env_for {
  project=$1
  echo 'Attempt to create a venv for' ${project} 'project'
  check_python
  [[ -n "${MAIN_PYTHON_FOUND}" ]] && create_env $project ${MAIN_PYTHON_FOUND} || { echo "Quitting, not python found, please install it"; exit 1; }
}

function run_project {
  project_name=$1
  echo 'Attempt to run' ${project_name} 'project'
  source venv/bin/activate
  python3 'main_'$project_name'.py'
}