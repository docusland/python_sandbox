#!/usr/bin/bash
source ./utils.sh

function setup {

  find_py_project

  echo '|    Found project    |    Entry point        |'
  echo '|---------------------|-----------------------|'
  # shellcheck disable=SC2068
  for project in ${!PY_PROJECT_FOUND[@]}
  do
    printf "| %-20s| %-20s  |\n" $project ${PY_PROJECT_FOUND[$project]}
  done

  echo -n "What project ?"
  read project_name


  create_env_for $project_name

  run_project $project_name


}

setup



