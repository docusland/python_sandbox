# Import the library tkinter
from tkinter import *

# Create a GUI app
app = Tk()

# Give a title to your app
app.title("Vinayak App")

# Constructing the first frame, frame1
frame1 = LabelFrame(app, text="Fruit", bg="green",
                    fg="white", padx=15, pady=15)

# Displaying the frame1 in row 0 and column 0
frame1.grid(row=0, column=0)

# Constructing the button b1 in frame1
b1 = Button(frame1, text="Apple")

# Displaying the button b1
b1.pack()

# Constructing the second frame, frame2
frame2 = LabelFrame(app, text="Vegetable", bg="yellow", padx=15, pady=15)

# Displaying the frame2 in row 0 and column 1
frame2.grid(row=0, column=1)

# Constructing the button in frame2
b2 = Button(frame2, text="Tomato")

# Displaying the button b2
b2.pack()

# Make the loop for displaying app
app.mainloop()


# def one_frame(self):
#     print('one')
#     frame1 = tk.Frame(self.root)
#     frame1.grid(row=0, column=0, sticky="nsew")
#     frame1.grid_columnconfigure(0, weight=1)  # left padding
#     frame1.grid_columnconfigure(1, weight=0)
#     frame1.grid_rowconfigure(0, weight=0)
#     frame1.grid_rowconfigure(1, weight=1)
#
#     left_text = tk.Text(master=frame1, font='Calibri 10')
#     left_text.grid()


def split_window(self):
    self.clear_frame()

    left = tk.LabelFrame(self.root, bg="green")
    left.grid(row=0, column=0)

    # actions = tk.Listbox(left, width=20)
    # actions.insert(1, tk.Button(master=self.root, text='un'))
    # actions.insert(2, tk.Button(master=left, text='deux'))
    # actions.insert(3, tk.Button(master=left, text='trois'))
    # actions.pack()

    # Constructing the second frame, frame2
    frame2 = tk.LabelFrame(self.root, text="Vegetable", bg="yellow")

    # Displaying the frame2 in row 0 and column 1
    frame2.grid(row=0, column=1)

    # Constructing the button in frame2
    b2 = tk.Button(frame2, text="Tomato")

    # Displaying the button b2
    b2.pack()

    # left = tk.Frame(highlightbackground='red')
    # left.grid(row=0, column=0, sticky="nsew")
    # left_text = tk.Text(master=left, font='Calibri 10')
    # left_text.insert(tk.END, 'hehe')
    # left_text.grid()
    # left.grid(sticky="nsew")
    # actions = tk.Listbox(left, width=20)
    # actions.insert(1, tk.Button(master=left, text='un'))
    # actions.insert(2, tk.Button(master=left, text='deux'))
    # actions.insert(3, tk.Button(master=left, text='trois'))

    # right = tk.Frame(bg='blue')
    # right.grid(row=0, column=0, sticky="nsew")
    # right_text = tk.Label(master=right, font='Calibri 10')
    # right_text.grid()

    # Create first frame with list on the left and padding on the right
    # frame1 = tk.Frame(self.root)
    # frame1.grid(row=0, column=0, sticky="nsew")
    # frame1.grid_columnconfigure(0, weight=1)  # left padding
    # frame1.grid_columnconfigure(1, weight=0)
    # frame1.grid_rowconfigure(0, weight=0)
    # frame1.grid_rowconfigure(1, weight=1)
    #
    # left_text = tk.Text(master=frame1, font='Calibri 10')
    # left_text.grid()
    # frame1_list = tk.Listbox(frame1, width=20)
    # frame1_list.insert(1, "Item 1")
    # frame1_list.insert(2, "Item 2")
    # frame1_list.insert(3, "Item 3")
    # frame1_list.grid(row=0, column=0, sticky="nsew", padx=10, pady=10)
    # frame1_padding = tk.Frame(frame1)
    # frame1_padding.grid(row=0, column=1, sticky="nsew", padx=0, pady=0)

    ###############
    # frame1 = tk.Frame(self.root, bg="blue", highlightbackground="red", highlightthickness=2, bd=0)
    # frame1.grid(row=0, column=0, sticky="nsew", padx=10, pady=10)
    #
    # frame1.grid_columnconfigure(0, weight=1)
    # frame1.grid_columnconfigure(1, weight=1)
    # frame1.grid_rowconfigure(0, weight=1)
    # frame1.grid_rowconfigure(1, weight=1)
    #
    # frame1_padding = tk.Frame(frame1)
    # frame1_padding.grid(row=0, column=1, sticky="nsew", padx=10, pady=10)
    #
    # text1 = tk.Label(master=frame1, text='left', font='Calibri 10')
    # text1.grid()
    #
    # frame2 = tk.Frame(self.root)
    # frame2.grid(row=0, column=1, sticky="nsew", padx=10, pady=10)
    ##############

    # Create second frame with Lorem Ipsum text on the right
    # frame2 = tk.Frame(self.root)
    # frame2.grid(row=0, column=1, sticky="nsew")
    # frame2.grid_columnconfigure(0, weight=1)
    # frame2.grid_rowconfigure(0, weight=1)
    # frame2_text = tk.Text(frame2, wrap="word")
    # frame2_text.insert(tk.END,
    #                    "Lorem ipsum dolor sit ame\nt, consectetur adipiscing e\nlit, oris nisi ut aliquip\n ex ea commodo consequat.")
    # frame2_text.configure(state="disabled")
    # frame2_text.grid(row=0, column=0, sticky="nsew", padx=10, pady=10)
