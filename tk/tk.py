import tkinter as tk


class MyApp:

    def __init__(self):
        self.name = 'Gibson'

    def run(self):
        a = TKUserInterface()
        a.init_main_window(self.name)


class MyFrameI:

    def clear_frame(self):
        ...

    def build(self):
        ...


class MyFrame(MyFrameI):
    """
    build a sub frame in a tk root
    """

    def __init__(self, _root):
        self.root = _root

    def clear_frame(self):
        """
        clear all widgets from main windows # TODO user tk.Frame()
        :return:
        """
        print(self.root)
        for widgets in self.root.winfo_children():
            print(widgets)
            # widgets.destroy()


class NewWindow(MyFrame):
    def __init__(self, _root):
        super().__init__(_root)
        self.frame = tk.Frame(self.root)
        self.quitButton = tk.Button(self.frame, text='Quit', width=25, command=self.close_windows)
        self.quitButton.grid()
        self.frame.grid()

    def close_windows(self):
        self.root.destroy()


class LeftMenuFrame(MyFrame):
    def __init__(self, _root, _commands):
        super().__init__(_root)
        self.frame = tk.Frame(self.root)
        self.commands = _commands

    def build(self):
        _ = tk.Frame(self.root)
        self.clear_frame()
        actions = [tk.Button(_, text=command[0], command=command[1]) for command in self.commands]
        for but in actions:
            but.pack()

        return _


class RightContent(MyFrame):
    def __init__(self, _root, content):
        super().__init__(_root)
        self.frame = tk.Frame(self.root)

    def build(self):
        _ = tk.Frame(self.root)


class TKUserInterface:

    def __init__(self):
        self.root = tk.Tk()

    def init_main_window(self, _title):
        """
        create tk window
        :return:
        """
        self.root.title(_title)
        self.root.geometry("700x350")

        # self.root.grid_rowconfigure(0, weight=0)
        # self.root.grid_columnconfigure(0, weight=1)

        main_frame1 = tk.Frame(bg='red')
        main_frame2 = tk.Frame(bg='black')
        #
        text1 = tk.Button(master=main_frame1, text="app new window", font='Calibri 10', command=self.open_new_window)
        text1.grid()

        text2 = tk.Button(master=main_frame2, text="app 2x frame", font='Calibri 10', command=self.split_window)
        text2.grid()
        #
        main_frame1.grid(row=0, column=0)
        main_frame2.grid(row=1, column=0)
        # self.split_window()
        self.root.mainloop()

    # def do_something_with_button(self):
    #     """
    #     create a game & ask for nb of players, => send to go()
    #     :return:
    #     """
    #     self.clear_frame()

    def split_window(self):
        commands = [('command1', lambda x=1: print(x)), ('command2', lambda x=2: print(x))]
        leftmenu = LeftMenuFrame(_root=self.root, _commands=commands).build()
        leftmenu.grid(row=0, column=0)
        # RightContent(_root=self.root, content='bob').build()

    def open_new_window(self):
        _newWindow = tk.Toplevel(self.root)
        NewWindow(_newWindow)

    # def clear_frame(self):
    #     """
    #     clear all widgets from main windows # TODO user tk.Frame()
    #     :return:
    #     """
    #     for widgets in self.root.winfo_children():
    #         widgets.destroy()


if __name__ == '__main__':
    app = MyApp()
    app.run()
