from datetime import datetime

from flask import Flask, render_template, request, redirect, url_for

from flask_light_template.db import DB


class Server:

    def __init__(self, _name, _host, _port):
        self.__name = _name
        self.__host = _host
        self.__port = _port
        self.__app = None
        self.db = DB()
        self.last_update = 'nope'

    @property
    def host(self):
        return self.__host

    @property
    def port(self):
        return self.__port

    @property
    def name(self):
        return self.__name

    def create_app(self):
        self.__app = Flask(__name__)

    def set_routes(self):
        @self.__app.route('/', methods=['GET'])
        def _home():
            return render_template('home.html', last_update=self.last_update)

        @self.__app.route('/add', methods=['GET', 'POST'])
        def form_add():
            if request.method == 'POST':
                print(request)
                nom = request.form['nom']
                prenom = request.form['prenom']

                self.db.insert_into({'nom': nom, 'prenom': prenom})
                _now = datetime.now()
                self.last_update = f'{_now.day}/{_now.month}/{_now.year} - {_now.hour}:{_now.minute}:{_now.second}'
                return redirect(url_for('_home'))
            return render_template('add.html', last_update=self.last_update)

        @self.__app.route('/ssti', methods=['GET', 'POST'])
        def ssti():
            print(request.get_data())
            return render_template('ssti.html', _payload=request)


        @self.__app.route('/diplay_list', methods=['GET'])
        def get_list():
            return render_template('list.html', last_update=self.last_update, items=self.db.select())

    def run(self):
        self.create_app()
        self.set_routes()
        self.__app.run(self.host, self.port)


if __name__ == '__main__':
    server = Server(_name='gibson', _host='0.0.0.0', _port=17117)
    server.run()
