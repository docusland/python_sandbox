<div align="right">

![](dropbox/made-with-python.svg)

</div>


# Python sandbox
<br>

    Be aware, its a sandbox
    Python3.X, no requirements, no test, no __init__ :^)
<br>

## flask light template

````shell
cd
python Server.py
# go to http://127.0.0.1:17117/
# add some ppl to the list
````
## socket server

````shell
cd socket_server
python main.py
````

<ins>Usage : </ins>

````shell
cd socket_server
python client.py.py
````

- [X] Server
```sh
SocketServer - INFO     - server     -> create_socket       : Socket set to : <socket.socket fd=408, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('0.0.0.0', 1117)>
SocketServer - INFO     - server     -> start               : SocketServer running
SocketServer - INFO     - server     -> listening           : Received < info > from 127.0.0.1:1
SocketServer - INFO     - server     -> listening           : Received < gibson > from 127.0.0.1:1
SocketServer - INFO     - server     -> listening           : Received < hour > from 127.0.0.1:1
SocketServer - WARNING  - server     -> listening           : Timeout: 127.0.0.1:1
SocketServer - INFO     - server     -> listening           : Received < python > from 127.0.0.1:3
SocketServer - INFO     - server     -> listening           : Received < stop > from 127.0.0.1:4
SocketServer - INFO     - server     -> stop                : Server stopped

Process finished with exit code 0
```

- [X] Client
```sh
Sending < info > to 127.0.0.1
Received: < Server info... >
Sending < gibson > to 127.0.0.1
Received: < SG 🤘 >
Sending < hour > to 127.0.0.1
Received: < Thursday 15 June 2023 23h51m59s >
Sending <  > to 127.0.0.1
Received: < ????? >
Sending < python > to 127.0.0.1
Received: < Invalid command >
Sending < stop > to 127.0.0.1
Received: < Stopping server... >
```

<br>


### dropbox/random mouse
```sudo apt-get install python3.10-tk python3-dev```


### ...

<details>
<summary>huho</summary>


# Title

<div style="height=500px">

<div style="width:50%; float:left">

```mermaid

graph TD

    Eucaryotes --> Plantae & Fungi & Animalia
    Plantae --> Parent1 & Parent2
    Parent1 --> Enfant1:::p & Enfant2 & Enfant3
    Parent2 --> Enfant4
    Enfant1 --> Plante2:::p
    Enfant1 --> Plante3:::p

    classDef p fill:#FF9A00
```

</div>
<div style="width:50%; float:left">

```mermaid

graph TD

    Eucaryotes --> Plantae & Fungi & Animalia
    Plantae --> Parent1 & Parent2
    Parent1 -->  Enfant2 & Enfant3
    Parent2 --> Enfant4
    Enfant1 --> Plante2:::p
    Enfant1 --> Plante3:::p
    Enfant2 --> Enfant1:::p 

    classDef p fill:#FF9A00
```
</div>
</div>

<div align="center">

# This is gonna be centered!

</div>


```mermaid

graph TB
    subgraph Supervised_ML
        subgraph data 

            subgraph dataset
                f["X features\nY Target"]:::_data
            end
            Parsing
            Gathering
        end

        subgraph AI 

            subgraph data_process
                load_data
                split_data
            end

            subgraph training
                subgraph train_data
                    f_train["80%\nX features\nY Target"]:::_data
                end
                train_data -- "modele.fit(x_train, y_train)" --> trained_model
            end

            subgraph validation
                subgraph test_data
                    f_test["20%\nX features\nY Target"]:::_data
                end
                test_data --"evaluate modele.predict(x_test) & y_test"--> trained_model2
                trained_model2["trained_model"]-->confusion & precision & recall & f1score
                confusion:::valid_metric
                precision:::valid_metric
                recall:::valid_metric                
                f1score:::valid_metric

            end
            
            
        end
        trained_model -.-> trained_model2
        subgraph Deploy 
            final["final_model.predict(real_input)"]
        end
        
    end
    
    Gathering --> Parsing --> dataset
    dataset --normalize--> load_data --> split_data
    split_data --0.2--> test_data
    split_data --0.8--> train_data 

    confusion & precision & recall & f1score --> final 
    
    style trained_model fill:#52ff33
    style trained_model2 fill:#52ff33
    style final fill:#5691ff
    classDef valid_metric fill:#fd6fa3
    classDef _data fill:#fbdf2a


```


```mermaid

graph LR

subgraph Notes
    B[Value Function]
    H[Q-Values]
    J[Learning Algorithm]
end

subgraph Reinforcement
    A[Agent]
    E[Environment]
    C[Observer]
    D[Policy]
    F[State]

end

C -- Observes --> F
E -- Provides observations and rewards --> C
A -- interact --> E
D -. Maps states to actions -.- F -.- A
C -- Reward --> A
C <-- π* --> D
```


```mermaid

graph LR
    
    subgraph Notes
        V[Value Function]
        Q[Q-Values]
        Alg[Learning Algorithm]
        Markov
        Trade-off
        PG["Policy Gradient"]
        RB["Replay Buffer"]
    end
    
    subgraph Reinforcement
        A[Agent]
        E[Environment]
        O[Observer]
        P[Policy]
        S[State]
    end
    
    O -- Observes --> S
    O -- Provides observations and rewards --> E
    A -- interact --> E
    P -. Maps states to actions -.- S --> A
    O -- Reward --> A
    O <-- π* --> P
```

</details>

