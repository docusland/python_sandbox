import logging


def init(_name='root', _level=logging.INFO):
    log_format = logging.Formatter('%(name)-8s - %(levelname)-8s - %(module)-10s -> %(funcName)-20s: %(message)s')
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_format)

    _log = logging.getLogger()
    _log.name = _name
    _log.setLevel(_level)

    # TODO
    # logging.getLogger('urllib3').setLevel(logging.INFO)
    # logging.getLogger('werkzeug').setLevel(logging.WARNING)
    # logging.getLogger('matplotlib.font_manager').setLevel(logging.INFO)
    # logging.getLogger('PIL.PngImagePlugin').setLevel(logging.INFO)
    ################

    logging.getLogger().addHandler(stream_handler)