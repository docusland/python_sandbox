import socket


class Client:
    def __init__(self, server_address):
        self.server_address = server_address
        self.client_socket = None

    def send_message(self, message, other_port=None):
        # TODO security
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.bind(('', 1 if not other_port else other_port))
        self.client_socket.connect(self.server_address)
        print(f'Sending < {message} > to {self.server_address[0]}')
        self.client_socket.sendall(message.encode())
        # try:
        data = self.client_socket.recv(1024).decode()
        # except ConnectionResetError :  [WinError 10054] An existing connection was forcibly closed by the remote host
        print('Received:', f"< {data} >" if data else 'Nothing append')
        self.client_socket.close()


def user_story():
    _server_address = ('127.0.0.1', 1117)
    _client = Client(_server_address)
    _client.send_message('info')
    _client.send_message('gibson')
    _client.send_message('hour')
    _client.send_message('')
    _client.send_message('python', other_port=3)
    _client.send_message('stop', other_port=4)  # TODO


if __name__ == '__main__':
    user_story()
