import logging
import sys
import socket
import socket_server.logger as logger


class SocketServer:

    def __init__(self):
        logger.init('SocketServer')
        self.is_running = False
        self.conn = None

    @staticmethod
    def create_conn(host='localhost', port=11117):
        """
        Create a socket object and bind it to a specific IP address and port.
        Address Families:
        AF_INET: IPv4 address family.
        AF_INET6: IPv6 address family.
        AF_UNIX or socket.AF_LOCAL: Unix domain sockets for communication within the same machine.
        Socket Types:
        SOCK_STREAM: Stream-oriented socket, providing a reliable, two-way communication using TCP.
        SOCK_DGRAM: Datagram socket, providing an unreliable, connectionless communication using UDP.
        SOCK_RAW: Raw socket, allowing direct access to lower-level protocols.
        SOCK_SEQPACKET: Sequenced packet socket, which provides a reliable, sequenced, and unduplicated data transmission.
        """
        try:
            conn = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
            conn.bind((host, port))
            logging.info(f"Listening on < {host}:{port} >")
        except socket.error as err:
            logging.info("Failed to create connection :", str(err))
            sys.exit()
        return conn

    def start(self):
        """
        Start the server
        Create conn
        """
        self.conn = self.create_conn()
        self.conn.listen()
        logging.info('Conn is listening...')
        self.is_running = True
        client_socket, client_address = self.conn.accept()
        logging.info(f'Connected {client_address}')
        while self.is_running:
            data = client_socket.recv(16)
            if data:
                logging.info(f'data : {data}')
            # if not data:
            if data in ['stop', b'stop']:  # TODO
                logging.info('stop')
                self.stop()
                client_socket.close()
                break

    def stop(self):
        """
        Close connection
        Stop the server
        """
        logging.info('running = False')
        self.is_running = False
        self.conn.close()


if __name__ == '__main__':
    server = SocketServer()
    server.start()
