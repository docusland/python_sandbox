import socket
import logging
import socket_server.logger as logger


class Client:
    def __init__(self, username='bob'):
        logger.init('SocketClient')
        self.username = username
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def send(self, _msg='Gibson'):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        server_address = ('127.0.0.1', 11117)
        client_socket.connect(server_address)

        message = f'{self.username} says {_msg} :^)!'
        client_socket.sendall(message.encode())

        # Receive
        # data = client_socket.recv(1024).decode()
        # print('Received:', data)

        client_socket.close()


def process():
    c = Client()
    c.send('huho')
    # c.send('sg')


if __name__ == '__main__':
    process()
