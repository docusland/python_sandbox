# GIT

## Huho

*ctrl f huho = TODO*

Prenons par pur hasard Alice et Bob, parce que ca fait A et B. Et on ajoute Charlie si besoin :^)

Alice veut developper une application.

## Git c'est quoi

Versionning, garde historique changements & versions

J'ai un fichier, je sauvergade mes changements quand c'est ce que je veux à un INSTANT T / quand ca FAIT ce que je veux / quand ca FONCTIONNE /.

## Exemple Noob (mais faut passer par la) : Faire son CV

### Les bases

Alice crée un dossier "mes_documents_pro".

```sh
# Dans "mes_documents_pro"
git init 
```
<details>
<summary>Plus d'infos sur ce qui vient de se passer</summary>

> Git prend connaissance des fichiers presents a la racine du projet
> et initialise un dossier .git (dont le fichier .git/config)
> Alice a créé un dossier "mes_documents_pro" dans lequel elle a créé son fichier CV.txt
> La racine du projet c'est le dossier "mes_documents_pro"
> git prend connaissance du fichier CV.
> Git fonctionne avec un système de branche (expliqué plus tard). 
> Initialement, une branche nommée master est crée. 
> En anglais, une controverse a été menée pour les termes master & slave (Git, Ansiblen Réplication de base de données)
> Ainsi microsoft a décidé de renommer cette branche "par défaut" en main. D'où la différence entre gitlab et github.
> Le nommage de cette branche par défaut peur être surchargée par de la config git
> - `git config --global init.defaultBranch main`

</details>




Alice crée un fichier "CV.txt" vide dans "mes_documents_pro".

```sh
# Dans "mes_documents_pro"
git add CV.txt 
```
<details>
<summary>Plus d'infos sur ce qui vient de se passer</summary>

> Git comprends que le fichier CV.txt est nouveau comparé à l'etat précedent.
> La création du fichier CV.txt est enregistrée.
</details>


```sh
# Dans "mes_documents_pro"
git commit -m "initial commit"
```
<details>
<summary>Plus d'infos sur ce qui vient de se passer</summary>

> Git enregistre ce changement d'état avec le message précédent de l'option -m
> Le premier commit d'un dépôt est souvent "Initial commit" ou "🎉 Initial commit"
</details>

Alice renseigne ensuite son profil & ses coordonnées dans son fichier CV.txt et souhaite enregistrer ce nouvel état du fichier.

```sh
# Dans "mes_documents_pro"
git add CV.txt 
# Git comprends que le fichier CV.txt doit etre enregistré comme étant modifié.

# Dans "mes_documents_pro"
git commit -m "Ajout des coordonnées & du profil"
# Git enregistre ce changement avec le message précédent de l'option -m
```

Alice ajoute ensuite ses expériences professionnelles puis git add, git commit.

On revient a nos branches :

```mermaid
gitGraph
    commit id: "initial commit"
    commit id: "Ajout des coordonnées & du profil"
```

Voilà, vous avez compris GIT mais pour le moment tout a fonctionné en local et Alice souhaite montrer les infos à son ami Bob.
Et git permets également de travailler à plusieurs. 
Pour celà Alice doit publier son travail sur une plateforme (github, gitlab, framagit ...)

Disons qu'elle dispose d'un dépôt privé chez gitlab nommé "mes_documents_pro".
Elle pourra faire 
```sh
# ici USER est une variable d'environnement, et correspond à son pseudo gitlab
# USER=MyNickIsAlice
git remote add origin git@gitlab.com:$USER/mes_documents_pro.git
```
<details>
<summary>Plus d'infos sur ce qui vient de se passer</summary>

> git sait maintenant que ce dossier est associé à un dépôt distant hébergé chez gitlab
> Protocole SSH privilégié aujourd'hui. Evitez le HTTPS.
> on lui a attribué l'alias 'origin' avec l'url spécifiée
> - cf .git/config

</details>

Puis 

```sh
git push origin master
```
<details>
<summary>Plus d'infos sur ce qui vient de se passer</summary>

> git envoie l'ensemble des commits locaux au dépot distant 
> associé à l'alias origin sur la branche master

</details>

Bob pourra alors récupérer ce travail en réalisant 


```sh
git clone git@gitlab.com:MyNickIsAlice/mes_documents_pro.git
```
<details>
<summary>Plus d'infos sur ce qui vient de se passer</summary>

> git copie l'ensemble des fichiers du gitlab vers un dossier local nommé mes_documents_pro
> git copie aussi tout l'historique de modifications des fichiers.
> pour celà il s'est défini un dossier .git également
> il a défini par défaut le dépot distant gitlab en tant que origin
</details>

(Vous pouvez tout simplement simuler bob en réalisant les actions dans un autre dossier de votre ordinateur).

Bob souhaite apporter une modification au texte et rajouter un bloc "hobbies" puis souhaite envoyer les modifications

```sh
# Dans "mes_documents_pro"
git add CV.txt 
# Git tracke toute les modifications du fichiers CV.

# Dans "mes_documents_pro"
git commit -m "Ajout des hobbies"
# Git enregistre ce changement avec le message précédent de l'option -m

git push origin master
```
Et informe Alice des changements, elle pourra les récupérer en local en faisant : 
```sh
git pull origin master
```
<details>
<summary>Plus d'infos sur ce qui vient de se passer</summary>

> git regarde s'il y a eu des changement sur la branche master de origin entre celui de gitlab et le sien en local
> git fetch ces commits, c'est à dire qu'il les télécharge et les stocke dans son cache
> git merge ces commits, c'est à dire qu'il les fusionne avec les fichiers d'Alice
> C'est là qu'on commence à pouvoir rencontrer les fameux merge conflicts... On verra ça plus tard.

</details>

Si elle regarde l'historique elle aura aussi les modifications apportées par bob.

```sh
git log
```

```mermaid
gitGraph
    commit id: "initial commit"
    commit id: "Ajout des coordonnées & du profil"
    commit id: "Ajout des hobbies"
```

## Afin de travailler à plusieurs

Afin de ne pas trop se gếner, ou tout du moins pouvoir travailler en toute quiétude. On a tendance à passer par un mécanisme de fork et merge requests.

Ce mécanisme est réalisé par la plateforme citée. 

Bob, va donc aller sur gitlab, au sein du dépôt d'Alice et cliquer sur le bouton Fork (en haut à droite). 
Celà va réaliser l'équivalent du git clone vu précédemment, mais ne va pas le télécharger vers un dossier en local,
Mais plutôt dupliquer le dépot dans les dépots gitlab de bob, que bob pourra télécharger.

```sh
git clone git@gitlab.com:MyNickIsBob/mes_documents_pro.git
```

Il pourra travailler et réaliser ses git add , git commit , git push etc ...
Une fois satisfait par le travail, il peut réaliser une merge request.
C'est à dire envoyer l'ensemble de ses commits de modification à Alice afin qu'elle puisse les voir et les valider. 

Encore une fois, il faudra aller sur la plateforme de gitlab et cliquer sur Merge request.
Pour celà il devra spécifier la branche qu'il souhaite envoyer et spécifier la branche destinatrice dans le dépôt d'Alice.

Alice sera notifiée de la demande. 

Bob pourra continuer à travailler sur sa version. 
Celà peut lui arriver également de vouloir être mis à jour avec le dépôt d'Alice.
Il peut le faire en spécifiant un autre remote que l'origin. 
En le rajoutant en tant qu'autre repository par exemple alice. 

```shell
git remote add alice git@gitlab.com:MyNickIsAlice/mes_documents_pro.git
git pull alice master
```

## Afin d'essayer d'avoir des nommages cohérents
- git flow (https://git-flow.readthedocs.io/en/latest/)
- git conventions (https://buzut.net/cours/versioning-avec-git/bien-nommer-ses-commits)




## Exemple plus complet avec du Code

### Initalisation du projet

Alice souhaite créer une application qui permet à l'utilisateur de deviner un nombre en N coups. 

Exemple en python évidemment :^) *(cf cours python)*

```sh
mkdir guess_the_number
cd guess_the_number
git init
```

Tj utile d'avoir un README.md huho *(cf cours README.md / Markdown / Mermaid ?)*

```sh
# dans guess_the_number
touch README.md
git add README.md
git commit -m 'initial commit'
```

Dans le README.md *(cf cours Markdown)*

```md 
# Guess the number

Voici mon application

## How to Run

lorem ipsum

## Dropbox

lorem ipsum
```

```sh
# dans guess_the_number
git add README.md
git commit -m 'docs: :memo: README'
```

```sh
# dans guess_the_number
touch main.py
```

```python
# main.py
def guess_the_number():

    # Nb tries
    nb_tries = 10

    # The number user needs to guess
    random_number = 17

    # For loop for each try
    for nb_try in range(nb_tries):
        user_input = input('number : ')
        if user_input == random_number:
            print('Congratulations! You guessed the number.')
            break
        else:
            print('Try again')
    else:
        print(f'Sorry, you have run out of tries. The correct number was {random_number}.')


if __name__ == '__main__':
    guess_the_number()
```

```sh
# dans guess_the_number
git add main.py
git commit -m 'Création de la fonctionnalité guess_the_number'
```

On en est là : 

branche master : o----------------------------------o----------------------------------o
                 |                                  |                                  | commit "Création de la fonctionnalité guess_the_number"
                 |commit "initial commit"           |
                                                    | commit "doc"

### Partage du projet

```shell
git add remote origin git@gitlab.com:$USER/guess_the_number.git
git push
```

### Récupération du projet

```shell
git pull git@gitlab.com:$USER/guess_the_number.git
cd guess_the_number
python main.py
```

### Modifications


```mermaid
gitGraph
    commit id: "initial commit"
    checkout main
    commit id: "checkout"
    branch develop
    commit id: "update 1"
    checkout develop
    commit id: "update 2"
    checkout main
    merge develop
````
```
