# perf
import sys
import time
import tracemalloc

# maths
import random

import numpy as np
from scipy.optimize import linear_sum_assignment

import itertools


def perf_decorator(func):
    """
    simple performance decorator
    :param func:
    :return:
    """
    main_methods = ['main_process', 'brute_force1', 'brute_force2']

    def wrapper(*args, **kwargs):
        tracemalloc.start()
        separator, start_line = ('-' * 10, '') if func.__name__ in main_methods else (
            '-' * 5, '>>   ')

        if func.__name__ in main_methods:
            print('\n')
        print(f'{start_line}{separator}{func.__name__}{separator}')
        start = time.perf_counter()
        out = func(*args, **kwargs)
        global_memory, peak_memory = tracemalloc.get_traced_memory()
        end = time.perf_counter()
        print(f'{start_line}Func : {func.__name__}',
              f'Time : {end - start:.6f}s',
              sep=' - ')
        print(f'{start_line}Global memory usage: {global_memory}',
              f'Global memory usage formatted: {global_memory / 10 ** 6:.6f} mb',
              sep=' - ')
        print(f'{start_line}Peak memory usage: {peak_memory}',
              f'Peak memory usage formatted: {peak_memory / 10 ** 6:.6f} mb',
              sep=' - ')
        tracemalloc.stop()
        print(f'{start_line}-------------')
        return out

    return wrapper


def _my_print(_mat, name):
    """
    print a not truncated np array
    :param _mat:
    :param name:
    :return:
    """
    print(f'{name} : \n')
    for row in _mat:
        print(row)


def generate_data():
    """
    generate a notes np.array( (n, n-1) )
    :return:
    """
    # _nb_student = random.randint(6, 8) * 2
    _nb_student = 8
    print(f'Nb students : {_nb_student}')
    all_notes = np.zeros((_nb_student, _nb_student - 1))
    for id_student in range(_nb_student):
        for id_note in range(_nb_student - 1):
            all_notes[id_student][id_note] = random.randint(1, 10)

    # TODO handle the ctf incoming data
    # all_notes = np.array([[2., 1., 10.],
    #                       [7., 3., 5.],
    #                       [10., 7., 3.],
    #                       [9., 9., 8.]])
    #
    # all_notes = np.array([[3, 2, 1],
    #                       [1, 2, 3],
    #                       [3, 1, 2],
    #                       [2, 3, 1]])
    return all_notes, _nb_student


def generate_notes_mat():
    ...


def avg_notes_by_student(_mat):
    return np.average(_mat, axis=1, keepdims=True)


def std_notes_by_student(_mat):
    return np.std(_mat, axis=1, keepdims=True)


# @perf_decorator
def reshape_square_mat(_mat):
    """
    from np.array(n, n-1) to (n, n) with mat[i][j] = 0 if i == j
    :param _mat:
    :return:
    """
    _new_mat = np.zeros((_mat.shape[0], _mat.shape[0]))
    for i in range(_mat.shape[0]):
        # print(f'---------student {i} ---------')
        for stu, note in enumerate(_mat[i]):
            if i <= stu:
                stu += 1
            # if not (5 < stu < _mat.shape[0] - 10):
            # print(f'student {i} scored student {stu} with {note}')
            # else:
            #     print('.', end='')
            _new_mat[i][stu] += note
    return _new_mat


@perf_decorator
def scores(_mat):
    """
    math[i][j] = sum (student i grade for student j + student j grade for student i)
    :param _mat:
    :return:
    """
    _scores = np.zeros((_mat.shape[0], _mat.shape[0]))
    for i in range(_mat.shape[0]):
        for j in range(i + 1):
            _scores[i][j] = _scores[j][i] = _mat[i][j] + _mat[j][i]
    return _scores


@perf_decorator
def best_couple(_scores):
    """
    best couple, i.e. best score = sum (note i + note j)
    :param _scores:
    :return:
    """
    # for i in range(_scores.shape[0]):
    #     for j in range(_scores.shape[1]):
    # print(f'students {i} & {j} = {_scores[i][j]}')
    _max = np.max(_scores)
    current_couple = np.where(_scores == _max)
    print(f'max score : {np.max(_scores)} for couple {current_couple}')


@perf_decorator
def scipy_process(_notes):
    row_ind, col_ind = linear_sum_assignment(-_notes)
    pairs = list(zip(row_ind, col_ind))
    _final_pairs = []
    for i, (x, y) in enumerate(pairs):
        if x not in [pair[0] for pair in _final_pairs] and y not in [pair[1] for pair in _final_pairs]:
            _final_pairs.append((x, y))
            _final_pairs.append((y, x))
    return _final_pairs


def all_students_are_here(_pairs_list, _nb_student):
    """
    valid the output list
    :param _pairs_list:
    :param _nb_student:
    :return:
    """
    expected_sum = sum([_ + 1 for _ in range(_nb_student)])
    actual_sum = [sum(x) for x in zip(*_pairs_list)]
    assert len(_pairs_list) == _nb_student, 'pas le bon nombre de paires'
    assert expected_sum == actual_sum[0] == actual_sum[1], 'des paires ne sont pas completes'
    print('ok')


@perf_decorator
def brute_force1(n, notes_mat):
    _students = [i for i in range(1, n + 1)]
    combinations = []

    permut = itertools.permutations(_students, len(_students))

    for pairs in permut:
        zipped = zip(pairs, _students)
        combinations.append(list(zipped))
    print('comb : ', combinations)

    valid_pairs = list(filter(
        lambda sublist: not any(pair[0] == pair[1] for pair in sublist) and all(
            (pair[1], pair[0]) in sublist for pair in sublist),
        combinations
    ))
    print('valid : ', valid_pairs)

    reshape_mat = reshape_square_mat(notes_mat)
    scores_map = list(map(
        lambda sub: [reshape_mat[pair[0] - 1][pair[1] - 1] for pair in sub],
        valid_pairs
    ))
    final_all = list(zip(valid_pairs, scores_map))
    final_sum = list(zip(valid_pairs, [sum(score) for score in scores_map]))

    print('map : ', list(scores_map))
    print('final all : ', list(final_all))
    print('final sum: ', list(final_sum))
    final_list = max(list(final_sum), key=lambda score: score[1])
    print('final list: ', final_list)
    return final_list


@perf_decorator
def brute_force2(n, notes_mat):
    _students = [i for i in range(1, n + 1)]

    valid_pairs = list(filter(
        lambda sublist: not any(pair[0] == pair[1] for pair in sublist) and all(
            (pair[1], pair[0]) in sublist for pair in sublist),
        [list(zip(pairs, _students)) for pairs in itertools.permutations(_students, len(_students))]
    ))

    final_sum = list(zip(
        valid_pairs,
        [sum(score) for score in map(
            lambda sub: [reshape_square_mat(notes_mat)[pair[0] - 1][pair[1] - 1] for pair in sub],
            valid_pairs
        )]
    ))
    print(final_sum)

    return max(final_sum, key=lambda score: score[1])

@perf_decorator
def brute_force3(n, notes_mat):
    _students = [i for i in range(1, n + 1)]
    print(_students)
    combinations = []

    _permutations = itertools.permutations(_students, 2)
    for pairs in _permutations:
        zipped = zip(pairs, _students)
        combinations.append(list(zipped))
    print('comb : ', combinations)
    return -117


@perf_decorator
def main_process(_notes, standard_print=True):
    if not standard_print:
        np.set_printoptions(threshold=sys.maxsize)
        # print(avg_notes_by_student(notes))
        # print(std_notes_by_student(notes))
    print('notes : \n', _notes) if standard_print else _my_print(_notes, 'notes')

    reshape_mat = reshape_square_mat(_notes)
    print('reshape : \n', reshape_mat, end='\n\n') if standard_print else _my_print(reshape_mat, 'reshape_mat')

    scores_mat = scores(reshape_mat)
    print(scores_mat, end='\n\n') if standard_print else _my_print(scores_mat, 'scores_mat')

    # best_couple(scores_mat)
    pairs = scipy_process(scores_mat)
    return list(map(
        lambda pair: (pair[0] + 1, pair[1] + 1),
        pairs
    ))


if __name__ == '__main__':
    notes, nb_student = generate_data()

    # print(notes)
    # final_pairs = main_process(_notes=notes, standard_print=True)
    # print(f'main => {final_pairs}', len(final_pairs))
    # all_students_are_here(final_pairs, nb_student)

    # Test
    # final_pairs[0] = (17, 18)
    # all_students_are_here(final_pairs, nb_student)
    # final_pairs.pop(0)
    # all_students_are_here(final_pairs, nb_student)

    # for i in range(2, 10, 2):
    #     final = brute_force3(i, notes)
    #     print(f'brute{i}', final)

    final = brute_force3(nb_student, notes)
    print('brute2', final)
