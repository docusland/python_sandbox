def maximum_ratings(ratings):
    n = len(ratings)
    indices = [i for i in range(n)]
    indices.sort(key=lambda x: -sum(ratings[x]))
    pairings = []
    for i in indices:
        for j in range(n - 1):
            if ratings[j][i] and not any(pairings[k][1] == j for k in range(len(pairings)) if pairings[k][0] == i):
                print((i, j))
                pairings.append((i, j))
                break
    return pairings


def format_output(pairings):
    output = []
    for pairing in pairings:
        output.append("{},{}".format(*pairing))
    return ";".join(output)


def main():
    n = int(input().strip())
    print(n)
    ratings = []
    for i in range(n):
        print(i)
        ratings.append(list(map(int, input().strip().split())))
    pairings = maximum_ratings(ratings)
    print(format_output(pairings))


if __name__ == "__main__":
    main()
