from munkres import Munkres
mnkrs = Munkres()
matrix = []

for i, val1 in enumerate(data1):
    for j, val2 in enumerate(data2):
       if len(matrix) < i+1:
          matrix.append([])
       if len(matrix[i]) < j+1:
          matrix[i].append([])
       matrix[i][j] = val2 - val1 # or whatever you want to compare

if matrix:
    best_idxs = mnkrs.compute(matrix)
    for (k, l) in best_idxs:
       print(data1[k], data2[l])