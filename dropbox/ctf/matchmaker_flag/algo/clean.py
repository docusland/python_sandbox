# perf
import sys
import time
import tracemalloc

# maths
import random

import numpy as np
from scipy.optimize import linear_sum_assignment

import itertools


def generate_data():
    """
    generate a notes np.array( (n, n-1) )
    :return:
    """
    # _nb_student = random.randint(6, 8) * 2
    _nb_student = 16

    print(f'Nb students : {_nb_student}')
    all_notes = np.zeros((_nb_student, _nb_student - 1))
    for id_student in range(_nb_student):
        for id_note in range(_nb_student - 1):
            all_notes[id_student][id_note] = random.randint(1, 10)
    return all_notes, _nb_student


def perf_decorator(func):
    """
    simple performance decorator
    :param func:
    :return:
    """
    main_methods = ['main_process', 'brute_force1', 'brute_force2']

    def wrapper(*args, **kwargs):
        tracemalloc.start()
        separator, start_line = ('-' * 10, '') if func.__name__ in main_methods else (
            '-' * 5, '>>   ')

        if func.__name__ in main_methods:
            print('\n')
        print(f'{start_line}{separator}{func.__name__}{separator}')
        start = time.perf_counter()
        out = func(*args, **kwargs)
        global_memory, peak_memory = tracemalloc.get_traced_memory()
        end = time.perf_counter()
        print(f'{start_line}Func : {func.__name__}',
              f'Time : {end - start:.6f}s',
              sep=' - ')
        print(f'{start_line}Global memory usage: {global_memory}',
              f'Global memory usage formatted: {global_memory / 10 ** 6:.6f} mb',
              sep=' - ')
        print(f'{start_line}Peak memory usage: {peak_memory}',
              f'Peak memory usage formatted: {peak_memory / 10 ** 6:.6f} mb',
              sep=' - ')
        tracemalloc.stop()
        print(f'{start_line}-------------')
        return out

    return wrapper


def reshape_square_mat(_mat):
    """
    from np.array(n, n-1) to (n, n) with mat[i][j] = 0 if i == j
    :param _mat:
    :return:
    """
    _new_mat = np.zeros((_mat.shape[0], _mat.shape[0]))
    for i in range(_mat.shape[0]):
        # print(f'---------student {i} ---------')
        for stu, note in enumerate(_mat[i]):
            if i <= stu:
                stu += 1
            # if not (5 < stu < _mat.shape[0] - 10):
            # print(f'student {i} scored student {stu} with {note}')
            # else:
            #     print('.', end='')
            _new_mat[i][stu] += note
    return _new_mat


@perf_decorator
def brute_force(n, notes_mat):
    _students = [i for i in range(1, n + 1)]
    combinations = []

    permut = itertools.permutations(_students, len(_students))

    for pairs in permut:
        zipped = zip(pairs, _students)
        combinations.append(zipped)
    print('comb : ', len(combinations))
    #
    # valid_pairs = list(filter(
    #     lambda sublist: not any(pair[0] == pair[1] for pair in sublist) and all(
    #         (pair[1], pair[0]) in sublist for pair in sublist),
    #     combinations
    # ))
    # print('valid : ', valid_pairs)
    #
    # reshape_mat = reshape_square_mat(notes_mat)
    # scores_map = list(map(
    #     lambda sub: [reshape_mat[pair[0] - 1][pair[1] - 1] for pair in sub],
    #     valid_pairs
    # ))
    # final_all = list(zip(valid_pairs, scores_map))
    # final_sum = list(zip(valid_pairs, [sum(score) for score in scores_map]))
    #
    # print('map : ', list(scores_map))
    # print('final all : ', list(final_all))
    # print('final sum: ', list(final_sum))
    # final_list = max(list(final_sum), key=lambda score: score[1])
    # print('final list: ', final_list)
    # return final_list
    return 117


if __name__ == '__main__':
    notes, nb_student = generate_data()
    final = brute_force(nb_student, notes)
    print('brute : ', final)
