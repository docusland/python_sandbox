import itertools
import tracemalloc

import numpy as np
from pwn import *
from scipy.optimize import linear_sum_assignment


# https://docs.pwntools.com/en/stable/tubes/processes.html
# https://github.com/Gallopsled/pwntools-tutorial/blob/master/tubes.md


def perf_decorator(func):
    """
    simple performance decorator
    :param func:
    :return:
    """
    main_methods = ['main_process']

    def wrapper(*args, **kwargs):
        tracemalloc.start()
        separator, start_line = ('-' * 10, '') if func.__name__ in main_methods else (
            '-' * 5, '>>   ')
        if func.__name__ in main_methods:
            print('\n')
        print(f'{start_line}{separator}{func.__name__}{separator}')
        start = time.perf_counter()
        out = func(*args, **kwargs)
        global_memory, peak_memory = tracemalloc.get_traced_memory()
        end = time.perf_counter()
        print('>' * 20)
        print(f'{start_line}Func : {func.__name__}',
              f'Time : {end - start:.6f}s',
              sep=' - ')
        print(f'{start_line}Global memory usage: {global_memory}',
              f'Global memory usage formatted: {global_memory / 10 ** 6:.6f} mb',
              sep=' - ')
        print(f'{start_line}Peak memory usage: {peak_memory}',
              f'Peak memory usage formatted: {peak_memory / 10 ** 6:.6f} mb',
              sep=' - ')
        tracemalloc.stop()
        print('>' * 20)
        return out

    return wrapper


def generate():
    """
    generate data sample for testing
    """
    nb_student, stub = 8, ''
    for i in range(nb_student):
        for j in range(nb_student - 1):
            stub += str(random.randint(1, 100))
            if j < (nb_student - 2):
                stub += ' '
        stub = stub + '\n'
    return stub


def format_result(_pairs):
    """
    format best pairs as 1,2;2,1;3,5;5,3
    """
    # r = ''.join(map(
    #     # lambda pair: (pair[0] + 1, pair[1] + 1),
    #     lambda pair: f'{pair[0]},{pair[1]};',
    #     _pairs
    # ))
    resp = ""
    if len(_pairs) == 2:
        a = [f"{pair[0]},{pair[1]};" for pair in _pairs[0]]
        resp = resp.join(a).rstrip()
    else:
        for value in _pairs:
            resp += f"{value[0]},{value[1]};"
        resp = resp[:-1]
    return resp


class NetCat:
    """
    Net Cat Tool to send & receive message from remote server
    """

    def __init__(self, host, port) -> None:
        self.remote = remote(host, port)

    def get_message(self) -> str:
        return self.remote.recvuntil(b">", drop=True, timeout=18).decode(encoding="UTF-8")

    def send_message(self, message: str):
        self.remote.send(message.encode())


@perf_decorator
def logic_lsa(grade_table):
    """
    in : list[List[int]]
    step : np array (n, n-1)
    step : np array (n, n)
    step : np array scores
    step : lsa ie linear_sum_assignment
    out : best result
    """
    # TODO cast_np_array(), reshape()...
    nb_student = len(grade_table)
    print(f'LSA for {nb_student} students')

    # np array (n, n-1)
    grades_mat = np.zeros(shape=(nb_student, nb_student - 1))
    for id_student in range(len(grades_mat)):
        for id_note in range(len(grades_mat) - 1):
            grades_mat[id_student][id_note] = grade_table[id_student][id_note]
    print('grades mat :\n', grades_mat)

    # reshape in square mat with zeros diagonal
    final_grades_mat = np.zeros(shape=(nb_student, nb_student))
    for student in range(nb_student):
        for other, note in enumerate(grades_mat[student]):
            # fill diagonal with zeros
            if student <= other:
                other += 1
            final_grades_mat[student][other] += note
    print('final grades mat :\n', final_grades_mat)
    # TODO END

    # scores, mat[i][j] = (grade i to j) + (grade j to i)
    _scores = np.zeros((nb_student, nb_student))
    for i in range(nb_student):
        for j in range(i + 1):
            _scores[i][j] = _scores[j][i] = final_grades_mat[i][j] + final_grades_mat[j][i]
    print('scores :\n', _scores)

    # lsa
    row_ind, col_ind = linear_sum_assignment(-_scores)
    pairs = list(zip(row_ind, col_ind))
    _final_pairs = []
    for i, (x, y) in enumerate(pairs):
        if x not in [pair[0] for pair in _final_pairs] and y not in [pair[1] for pair in _final_pairs]:
            _final_pairs.append((x, y))
            _final_pairs.append((y, x))
    print('final pairs :\n', _final_pairs)

    return _final_pairs


@perf_decorator
def logic_brute_force(grade_table):
    """
    in : list[List[int]]
    step : permutations + comb
    step : oof
    out : best result
    """
    # TODO factorize
    nb_student = len(grade_table)
    print(f'Brute force for {nb_student} students')
    if nb_student > 9:
        assert False, 'NOPE'
    # np array (n, n-1)
    grades_mat = np.zeros(shape=(nb_student, nb_student - 1))
    for id_student in range(len(grades_mat)):
        for id_note in range(len(grades_mat) - 1):
            grades_mat[id_student][id_note] = grade_table[id_student][id_note]
    print('grades mat :\n', grades_mat)
    # reshape in square mat with zeros diagonal
    final_grades_mat = np.zeros(shape=(nb_student, nb_student))
    for student in range(nb_student):
        for other, note in enumerate(grades_mat[student]):
            # fill diagonal with zeros
            if student <= other:
                other += 1
            final_grades_mat[student][other] += note
    print('final grades mat :\n', final_grades_mat)
    
    
    
    # TODO tout ca c'est numpy.fromiter()
    _students = [i for i in range(nb_student)]

    valid_pairs = list(filter(
        lambda sublist: not any(pair[0] == pair[1] for pair in sublist) and all(
            (pair[1], pair[0]) in sublist for pair in sublist),
        [list(zip(pairs, _students)) for pairs in itertools.permutations(_students, len(_students))]
    ))

    final_sum = list(zip(
        valid_pairs,
        [sum(score) for score in map(
            lambda sub: [final_grades_mat[pair[0] - 1][pair[1] - 1] for pair in sub],
            valid_pairs
        )]
    ))
    # end
    print('final sum :', final_sum, sep='\n')
    print('final sum max :', max(final_sum, key=lambda score: score[1]), sep='\n')

    return max(final_sum, key=lambda score: score[1])


@perf_decorator
def handle_one_problem(_raw_student):
    """
    handling one problem
    in : raw list of grades
    out : best result
    """
    # parse into List[List[int]], shape (n, n - 1)
    grade_table = list(map(
        lambda student: [int(grade) for grade in student.split(' ')],
        _raw_student
    ))
    # TODO CHANGE ALGO

    for method in [logic_lsa, logic_brute_force]:
        pairs_debug = method(grade_table)
        print('\n\n\nDEBUG', format_result(pairs_debug))
    pairs = logic_lsa(grade_table)
    best_result = format_result(pairs)
    return best_result


def format_raw_input(_raw_input):
    _start = _raw_input[0:8]
    _end = (item for item in _raw_input[-8:])
    print(f'Formatting input [{_start}..{"".join(_end).rstrip()}]')

    # parse into List[str], len = nb student
    res = _raw_input.split("\n")
    # remove the last ''
    res.pop(len(res) - 1)
    return res


@perf_decorator
def loop(_counter, _server):
    """
    Loop method over 3 problems
    """
    print(f'-------- Problem {_counter}')
    # oof
    gibson = True
    student_input = generate() if gibson else _server.get_message()

    list_student = format_raw_input(student_input)

    # handling stuff
    pairs_output = handle_one_problem(list_student)
    print('pairs_output :', pairs_output, sep='\n')

    # get flag ?
    res = _server.send_message(pairs_output)
    print(f'{"FLAG = " if _counter == 2 else "res = "}{res}')
    return res


@perf_decorator
def main_process(_server):
    """
    Main method
    """
    for counter in range(3):
        res = loop(counter, _server)
        if not res:
            print('nope')
            break


def valid_lsa():
    """
    compare brute force and score(lsa_pairs)
    """
    ...

if __name__ == '__main__':
    nc = NetCat("0.cloud.chals.io", 22304)
    main_process(_server=nc)
