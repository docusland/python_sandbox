from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
    return 'index'


@app.route('/guitar')
def guitar():
    return {
        1: {
            'brand': 'gibson',
            'model': 'sg'
        },
        2: {
            'brand': 'gibson',
            'model': 'lespaul'
        },
        3: {
            'brand': 'fender',
            'model': 'telecaster'
        },
        4: {
            'brand': 'fender',
            'model': 'stratocaster'
        },
    }


if __name__ == '__main__':
    app.run(debug=True)
