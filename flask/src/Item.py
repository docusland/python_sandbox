class Item:

    def __init__(self, label: str = "default_label", price: int = 17):
        self.__label = label
        self.__price = price

    ########### GETTER & SETTER ###########

    def get_label(self):
        return self.__label

    def get_price(self):
        return self.__price

    def set_label(self, new_label):
        self.__label = new_label

    def set_price(self, new_price):
        self.__price = new_price

    ########### DEBUG ###########
    def show(self):
        print(f"Je suis un item {self.get_label()}, {self.get_price()}")
